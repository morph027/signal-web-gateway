+++
title = "Nextcloud 2FA"
+++

For detailed information see the [official docs](https://nextcloud-twofactor-gateway.readthedocs.io/en/latest/Admin%20Documentation/).

## Configuration

Configure your nextcloud instance to use the gateway:

```
root@nextcloud:~# sudo -u www-data -H php /var/www/nextcloud/occ twofactorauth:gateway:configure signal
Please enter the URL of the Signal gateway (leave blank to use default): https://signal-gateway.example.com/
Using https://signal-gateway.example.com/.
```

Now you can use setup for your user in the security settings section.
