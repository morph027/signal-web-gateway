+++
title = "Usage"
weight = 1
+++

### Multipart form data (w/ image upload)

You can just use curl to send messages using the following form data:

* *to*
* *message*
* *file* (optional)
* *group* (optional)

#### Plain text

Assuming you're running against the docker container at your machine:

```
curl -X POST -F "to=+1337" -F "message=Test" http://localhost:5000
```

#### Text message with attachement

Assuming you're running against the docker container at your machine:

```
curl -X POST -F file=@some-random-cat-picture.gif -F "to=+1337" -F "message=Test" http://localhost:5000
```

#### Send to groups

Assuming you're running against the docker container at your machine:

```
curl -X POST -F "to=ff702f10bebfa2f1508deb475ded2d65" -F "message=Test" http://localhost:5000
```

You can retrieve the groups hexid at `/groups` or by having a look into `.storage/groups`:

```bash
$ curl -s http://localhost:5000/groups | jq .
{
  "groups": {
    "f441...ad8a": {
      "name": "Foo"
    },
    "ccab...0257": {
      "name": "Bar"
    }
  }
}
```

### JSON data to specific path

If the app you're sending from for whatever reasons is unable to specify form values and just posts json webhooks (Influx Kapacitor, Gitlab webhooks, ...), you can post this data to a path containing the recipient (e.g. */+1337* or */ff702f10bebfa2f1508deb475ded2d65*).
The gateway then tries to send the value from json key defined in environment variable *JSON_MESSAGE* (defaults to *message*).

Example:

```
curl -X POST -d '{"message":"foo"}' http://localhost:5000/json/+1337
```

### Rekey in case of re-installing Signal

If you (or someone) has re-installed Signal (or switched to a new mobile), the Signal app and the servers will create new keys and this gateway refuses to send messages due to the changed key. You can send a *DELETE* request to */rekey/<recipient>* to delete the old key and receive messages again.

```
curl -X DELETE http://localhost:5000/rekey/491337420815
```
