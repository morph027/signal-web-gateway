# Signal Web Gateway

{{% notice warning %}}
**As i'm unable to get it working with latest upstream changes in golang/signal, this project is deprecated. New project based on signal-cli (Java) can be found [here](https://gitlab.com/morph027/signal-cli-dbus-gateway/).**
{{% /notice %}}

---

{{% notice info %}}
**This application has not been audited. It should not be regarded as
secure, use at your own risk. This is a third-party effort, and is NOT a part of the official Signal
project or any other project of Open Whisper Systems.**
{{% /notice %}}

This project is based upon [janimo's](https://github.com/janimo/textsecure) Signal client to allow other apps (reporting, monitoring, ...) sending private and encrypted Signal messages instead of e-mail or other proprietary and non-free protocols. Thanks to [nanu-c](https://github.com/nanu-c/textsecure/) for picking up the good work and fixing Signal protocol changes.

{{% notice warning %}}
**Security notice**:
You should run the gateway behind a reverse proxy like nginx to add tls/basic auth/acls or run this inside Docker swarm/Kubernetes with a dynamic ingress proxy like [Traefik](https://traefik.io) and [basic auth](https://docs.traefik.io/v2.0/middlewares/basicauth/).
{{% /notice %}}
