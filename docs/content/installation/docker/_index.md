+++
title = "Docker"
+++

## Prepare config and storage

* create docker volumes for the app:

```bash
docker volume create signal-web-gateway_config
docker volume create signal-web-gateway_storage
```

* if you do not already have a config file, just create one like this:

{{% notice note %}}
Registration could be done via SMS or Voice (e.g. for SIP numbers). **Do not use your current mobile phone number or it's account will be invalidated!**
Modify the config to suit your needs (e.g. add your phone number and select `voice` or `sms` as authentication method):
{{% /notice %}}

```yaml
#Put your phone number that will get verified by the server here
tel: "+1771111001"

#Server URL
server: https://textsecure-service.whispersystems.org:443

#Server's TLS root certificate path when using a non-default server
#See https://github.com/janimo/textsecure-docker#using-https
#rootCA: path/to/rootCA.pem

#proxy URL if you use one
#proxy: http://host:8080

#Registration code verification can be done via sms or voice
#Additionally the 'dev' type is supported for a non-official server fork.
verificationType: sms

#Where to store sessions' metadata, by default ".storage"
#storageDir: path/to/storage

#Setting this to false turns off encryption of local storage, for developing.
#unencryptedStorage: true

#The local storage uses password base encryption
#If not present here, the password will be requested on startup
storagePassword: password

#Overrides the default HTTP User-Agent field ("Go 1.1 package http")
#userAgent: "TextSecure Go command line client 0.1"

#Logging verbosity; valid values are error,warn,info and debug, case insensitive
#loglevel: debug
```

* copy into the volume using a little "workaround":

```bash
docker create -v signal-web-gateway_config:/signal/.config --name helper alpine:latest
docker cp config.yml helper:/signal/.config/
docker rm helper
```

## Register

```bash
docker run --rm -it -v signal-web-gateway_config:/signal/.config -v signal-web-gateway_storage:/signal/.storage registry.gitlab.com/morph027/signal-web-gateway:latest register
Enter verification code>
# confirm code
# ctrl+c
# ctrl+d
```

## Run

### Docker Compose

```bash
docker-compose -p signal-web-gateway up -d
```

### Docker

```bash
docker run -d --name signal-web-gateway --restart always -v signal-web-gateway_config:/signal/.config -v signal-web-gateway_storage:/signal/.storage -p 127.0.0.1:5000:5000 registry.gitlab.com/morph027/signal-web-gateway:latest
```
