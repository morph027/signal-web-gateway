+++
title = "Standalone"
+++

## Download

You can download the latest CI artifacts:

* [linux-amd64](https://gitlab.com/morph027/signal-web-gateway/-/jobs/artifacts/master/raw/signal-amd64?job=binary-amd64)
* [linux-arm7](https://gitlab.com/morph027/signal-web-gateway/-/jobs/artifacts/master/raw/signal-arm7?job=binary-arm7) (e.g. Raspberry Pi 3)
* [linux-arm7](https://gitlab.com/morph027/signal-web-gateway/-/jobs/artifacts/master/raw/signal-arm7?job=binaryarm64)

In addition to the binary, you need to download [libzkgroup](https://github.com/nanu-c/zkgroup/releases) to your local ld lib directory (e.g. `/lib`).

Or build from source code:

```bash
mkdir /tmp/docker
docker run --rm -it -v /tmp/docker:/build golang:latest bash
go get github.com/signal-golang/textsecure
go get github.com/go-redis/redis
cd src/github.com/signal-golang/textsecure/cmd/textsecure
go build
cp textsecure /build/signal
# ctrl + d
cp /tmp/docker/signal /usr/bin/
```

## Setup

### User

```
useradd -s /bin/bash -m signal
```

### Register

* edit the config like described [here](/installation/docker/#prepare-config-and-storage)
* run registration

Example:

```bash
cd /home/signal/
sudo -u signal -H signal
Enter verification code>
# confirm code
# ctrl+c
# ctrl+d
```

### Startup

#### Systemd

`/etc/default/signal-web-gateway`:

```
OPTS=""
```

`/etc/systemd/system/signal-web-gateway.service`:

```
[Unit]
Description=signal-web-gateway daemon
After=network.target
ConditionPathExists=/etc/default/signal-web-gateway

[Service]
PIDFile=/run/signal-web-gateway/pid
User=signal
Group=signal
RuntimeDirectory=signal-web-gateway
WorkingDirectory=/home/signal
EnvironmentFile=/etc/default/signal-web-gateway
ExecStart=/usr/bin/signal -gateway ${OPTS}
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

#### OpenRC (Alpine)

```
#!/sbin/openrc-run

depend() {
    need net
}

command="/usr/bin/signal"
command_args="-gateway"
command_user="signal:signal"
pidfile="/run/${RC_SVCNAME}/pid"
```

### Reverse proxy

As there are plenty of proxy or webservers (e.g. nginx, apache, caddy, traefik,...), just pick your favourite one.
